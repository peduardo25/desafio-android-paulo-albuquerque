package challenge.concrete.service.core.listener;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import rx.Observable;
import rx.subjects.PublishSubject;


public class RxRequestListener<T> implements RequestListener<T> {

    private final PublishSubject<T> mSubject = PublishSubject.create();


    public void onRequestFailure(SpiceException spiceException) {
        try {
            this.onError(spiceException);
        } catch (Throwable throwable) {
            //I would send this exception to crashlytics.
        }
    }

    @Override
    public final void onRequestSuccess(T result) {
        this.mSubject.onNext(result);
    }

    private void onError(final SpiceException e) {
        this.mSubject.onError(e);
    }

    public Observable<T> asObservable() {
        return mSubject;
    }

}
