package challenge.concrete.service.repositories.request;

import android.content.Context;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;

import javax.inject.Inject;

import challenge.concrete.service.core.request.AbstractRequest;
import challenge.concrete.service.repositories.RepositoryResource;
import challenge.concrete.service.repositories.result.PullRequestDataResult;

public class PullRRequest extends AbstractRequest<PullRequestDataResult.PullRequestDataList,
        RepositoryResource> {

    private String login;
    private String repository;

    @Inject
    public PullRRequest(Context context, SpiceManager spiceManager) {
        super(context, spiceManager, PullRequestDataResult.PullRequestDataList.class,
                RepositoryResource.class);
    }

    @Override
    protected String getCacheKey() {
        return PullRRequest.class.getName() + this.login + this.repository;
    }

    @Override
    protected long getDurationTime() {
        return DurationInMillis.ONE_MINUTE;
    }

    @Override
    public PullRequestDataResult.PullRequestDataList loadDataFromNetwork() throws Exception {
        return getService().getPullRequest(this.login, this.repository);
    }

    public PullRRequest with(final String login, final String repository) {
        this.login = login;
        this.repository = repository;

        return this;
    }
}