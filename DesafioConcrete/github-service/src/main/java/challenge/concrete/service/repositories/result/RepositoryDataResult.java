package challenge.concrete.service.repositories.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RepositoryDataResult {

    @SerializedName("total_count")
    private long totalCount;

    @SerializedName("items")
    private List<RepositoryItemResult> items;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<RepositoryItemResult> getItems() {
        return items;
    }

    public void setItems(List<RepositoryItemResult> items) {
        this.items = items;
    }
}
