package challenge.concrete.service.repositories;

import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryDataResult;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface RepositoryResource {

    @GET("/search/repositories")
    RepositoryDataResult getRepositories(
            @Query("q") String language,
            @Query("sort") String sort,
            @Query("page") int page
    );

    @GET("/repos/{owner}/{repo}/pulls")
    PullRequestDataResult.PullRequestDataList getPullRequest(
            @Path("owner") String login,
            @Path("repo") String repo
    );
}
