package challenge.concrete.service.core.request;

import android.content.Context;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

import challenge.concrete.service.core.listener.RxRequestListener;
import challenge.concrete.service.core.service.ChallengeRetrofitSpiceService;
import rx.Observable;


public abstract class AbstractRequest<RESULT, RESOURCE> extends RetrofitSpiceRequest<
        RESULT, RESOURCE> {

    public static final String NO_ITEM_FOUND = "No item found";
    private final Class<RESULT> cls;


    protected SpiceManager spiceManager;

    protected Context context;

    public AbstractRequest(final Context context, SpiceManager spiceManager, Class<RESULT> clazz,
                           Class<RESOURCE> retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
        this.cls = clazz;
        this.context = context;
        this.spiceManager = spiceManager;

        setRetryPolicy(new DefaultRetryPolicy(1,
                DefaultRetryPolicy.DEFAULT_DELAY_BEFORE_RETRY,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    public void execute(RequestListener<RESULT> listener) {
        this.spiceManager.start(context);
        this.spiceManager.execute(this, getCacheKey(), getDurationTime(), listener);
    }

    public Observable<RESULT> execute() {
        RxRequestListener<RESULT> mListener = new RxRequestListener<>();
        this.execute(mListener);
        return mListener.asObservable();
    }

    protected abstract String getCacheKey();

    protected abstract long getDurationTime();


}