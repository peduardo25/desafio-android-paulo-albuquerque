package challenge.concrete.service.repositories.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OwnerResult implements Parcelable {

    @SerializedName("id")
    private long id;

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("url")
    private String userUrl;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.login);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.userUrl);
    }

    public OwnerResult() {
    }

    protected OwnerResult(Parcel in) {
        this.id = in.readLong();
        this.login = in.readString();
        this.avatarUrl = in.readString();
        this.userUrl = in.readString();
    }

    public static final Parcelable.Creator<OwnerResult> CREATOR = new Parcelable.Creator<OwnerResult>() {
        @Override
        public OwnerResult createFromParcel(Parcel source) {
            return new OwnerResult(source);
        }

        @Override
        public OwnerResult[] newArray(int size) {
            return new OwnerResult[size];
        }
    };
}
