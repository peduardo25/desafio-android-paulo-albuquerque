package challenge.concrete.service.repositories.result;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;

public class PullRequestDataResult {

    @SerializedName("title")
    private String title;

    @SerializedName("state")
    private String state;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("user")
    private PRUserData user;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PRUserData getUser() {
        return user;
    }

    public void setUser(PRUserData user) {
        this.user = user;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public static class PullRequestDataList extends ArrayList<PullRequestDataResult> {
        public PullRequestDataList() {
        }

        public PullRequestDataList(Collection<? extends PullRequestDataResult> c) {
            super(c);
        }
    }
}
