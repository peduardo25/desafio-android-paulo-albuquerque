package challenge.concrete.service.core.service;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;


public class ChallengeRetrofitSpiceService extends RetrofitGsonSpiceService {

    private static final int CONNECT_TIMEOUT = 20000;
    private static final int READ_TIMEOUT = 20000;
    private static final int APPLICATION_THREAD_COUNT = 1;
    private static final String BASE_URL = "https://api.github.com";

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }

    @Override
    public int getThreadCount() {
        return APPLICATION_THREAD_COUNT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        final OkHttpClient httpClient = new OkHttpClient();
        final RestAdapter.Builder builder = super.createRestAdapterBuilder();

        httpClient.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        httpClient.setReadTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS);

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setClient(new OkClient(httpClient) {
            @Override
            public Response execute(Request request) throws IOException {
                return super.execute(request);
            }
        });
        return builder;
    }

}
