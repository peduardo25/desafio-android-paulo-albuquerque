package challenge.concrete.service.repositories.request;

import android.content.Context;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;

import javax.inject.Inject;

import challenge.concrete.service.core.request.AbstractRequest;
import challenge.concrete.service.repositories.RepositoryResource;
import challenge.concrete.service.repositories.result.RepositoryDataResult;

public class RepositoryRequest extends AbstractRequest<RepositoryDataResult, RepositoryResource>{

    private String language;
    private String sort;
    private int page;


    @Inject
    public RepositoryRequest(Context context, SpiceManager spiceManager) {
        super(context, spiceManager, RepositoryDataResult.class, RepositoryResource.class);
    }

    @Override
    protected String getCacheKey() {
        return RepositoryRequest.class.getName() + this.page;
    }

    @Override
    protected long getDurationTime() {
        return DurationInMillis.ONE_MINUTE;
    }

    @Override
    public RepositoryDataResult loadDataFromNetwork() throws Exception {
        return getService().getRepositories(this.language, this.sort, this.page);
    }

    public RepositoryRequest with(final String language, final String sort, final int page) {
        this.language = language;
        this.sort = sort;
        this.page = page;
        return this;
    }
}
