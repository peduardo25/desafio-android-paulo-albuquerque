package challenge.concrete.service.repositories.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RepositoryItemResult implements Parcelable {

    @SerializedName("id")
    private long id;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("stargazers_count")
    private int starsCount;

    @SerializedName("owner")
    private OwnerResult ownerResult;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public int getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(int starsCount) {
        this.starsCount = starsCount;
    }

    public OwnerResult getOwnerResult() {
        return ownerResult;
    }

    public void setOwnerResult(OwnerResult ownerResult) {
        this.ownerResult = ownerResult;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.fullName);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.forksCount);
        dest.writeInt(this.starsCount);
        dest.writeParcelable(this.ownerResult, flags);
    }

    public RepositoryItemResult() {
    }

    protected RepositoryItemResult(Parcel in) {
        this.id = in.readLong();
        this.fullName = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.forksCount = in.readInt();
        this.starsCount = in.readInt();
        this.ownerResult = in.readParcelable(OwnerResult.class.getClassLoader());
    }

    public static final Parcelable.Creator<RepositoryItemResult> CREATOR = new Parcelable.Creator<RepositoryItemResult>() {
        @Override
        public RepositoryItemResult createFromParcel(Parcel source) {
            return new RepositoryItemResult(source);
        }

        @Override
        public RepositoryItemResult[] newArray(int size) {
            return new RepositoryItemResult[size];
        }
    };
}
