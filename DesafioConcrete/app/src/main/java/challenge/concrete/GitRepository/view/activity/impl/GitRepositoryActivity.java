package challenge.concrete.GitRepository.view.activity.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import challenge.concrete.GitRepository.DaggerGitRepositoryComponent;
import challenge.concrete.GitRepository.RepositoryAction;
import challenge.concrete.GitRepository.presenter.GitRepositoryPresenter;
import challenge.concrete.GitRepository.view.ItemClickListener;
import challenge.concrete.GitRepository.view.activity.GitRepositoryView;
import challenge.concrete.GitRepository.view.adapter.RepositoryAdapter;
import challenge.concrete.GitRepository.view.component.EndlessRecyclerOnScrollListener;
import challenge.concrete.GitRepository.view.component.SimpleDividerItemDecoration;
import challenge.concrete.R;
import challenge.concrete.core.IdlingResource.SpiceManagerIdleResource;
import challenge.concrete.core.application.MobileApplication;
import challenge.concrete.service.repositories.result.RepositoryDataResult;
import challenge.concrete.service.repositories.result.RepositoryItemResult;

public class GitRepositoryActivity extends AppCompatActivity implements GitRepositoryView,
        ItemClickListener<RepositoryItemResult> {

    @Nullable
    @Inject
    SpiceManagerIdleResource mIdlingResource;

    @Inject
    GitRepositoryPresenter gitRepositoryPresenter;

    @Inject
    RepositoryAdapter repositoryAdapter;

    @BindView(R.id.rv_repositories)
    RecyclerView rvRepositories;

    @BindView(R.id.progress_repositories)
    ProgressBar progressFetchingData;

    @BindView(R.id.toolbar_repo)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repositories_screen);
        this.setupInject();
        this.setupButterKnife();

        this.setTitle(getString(R.string.home_title));
        setSupportActionBar(toolbar);

        this.setupRecyclerView();
        this.gitRepositoryPresenter.setView(this);
        this.progressFetchingData.setVisibility(View.VISIBLE);
        this.gitRepositoryPresenter.onViewCreated();

    }


    private void setupInject() {
        DaggerGitRepositoryComponent.builder()
                .appComponent(((MobileApplication) getApplication()).getAppInjection()).build()
                .inject(this);
    }

    private void setupButterKnife() {
        ButterKnife.bind(this);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                this.getApplicationContext()
        );

        this.rvRepositories.setAdapter(this.repositoryAdapter);
        this.rvRepositories.setLayoutManager(linearLayoutManager);

        this.rvRepositories.addOnScrollListener(this.getEndlessListener(linearLayoutManager));
        this.rvRepositories.addItemDecoration(
                new SimpleDividerItemDecoration(this.getApplicationContext())
        );

        this.repositoryAdapter.setItemClickListener(this);
    }

    @Override
    public void onRepositoriesLoaded(RepositoryDataResult repositoryDataResult) {
        this.progressFetchingData.setVisibility(View.GONE);
        this.repositoryAdapter.setRepositoryItemResults(repositoryDataResult.getItems());
        this.repositoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestError() {
        this.progressFetchingData.setVisibility(View.GONE);
        Toast.makeText(this.getApplicationContext(),
                getString(R.string.msg_repository_request_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPaginatedDataLoaded(RepositoryDataResult repositoryDataResult) {
        this.progressFetchingData.setVisibility(View.GONE);
        this.repositoryAdapter.getRepositoryItemResults().addAll(repositoryDataResult.getItems());
        this.repositoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadMoreDataRequestError() {
        this.progressFetchingData.setVisibility(View.GONE);
        Toast.makeText(this.getApplicationContext(),
                getString(
                        R.string.msg_repository_load_paginated_page_request_error
                ), Toast.LENGTH_LONG).show();
    }

    @Override
    public Bundle getArguments() {
        return this.getIntent().getExtras();
    }

    private EndlessRecyclerOnScrollListener getEndlessListener(
            final LinearLayoutManager linearLayoutManager) {
        EndlessRecyclerOnScrollListener listener = new EndlessRecyclerOnScrollListener(
                linearLayoutManager
        ) {
            @Override
            public void onLoadMore(int nextPage) {
                progressFetchingData.setVisibility(View.VISIBLE);
                gitRepositoryPresenter.onLoadMore(nextPage);
            }
        };

        return listener;
    }

    /**
     * Listener from user click on adapter.
     *
     * @param resId
     * @param item
     */
    @Override
    public void onItemClick(int resId, RepositoryItemResult item) {
        Intent intent = new Intent(RepositoryAction.Activity.PULL_REQUEST_ACTIVITY);
        Bundle bundle = new Bundle();
        bundle.putParcelable(RepositoryAction.REPOSITORY_KEY, item);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @VisibleForTesting
    @NonNull
    public SpiceManagerIdleResource getIdlingResource() {
        return mIdlingResource;
    }
}
