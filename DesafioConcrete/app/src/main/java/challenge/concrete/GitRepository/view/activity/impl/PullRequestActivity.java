package challenge.concrete.GitRepository.view.activity.impl;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import challenge.concrete.GitRepository.DaggerGitRepositoryComponent;
import challenge.concrete.GitRepository.presenter.PullRequestPresenter;
import challenge.concrete.GitRepository.view.ItemClickListener;
import challenge.concrete.GitRepository.view.activity.PullRequestView;
import challenge.concrete.GitRepository.view.adapter.PullRequestAdapter;
import challenge.concrete.GitRepository.view.component.SimpleDividerItemDecoration;
import challenge.concrete.R;
import challenge.concrete.core.application.MobileApplication;
import challenge.concrete.service.repositories.result.PullRequestDataResult;

public class PullRequestActivity extends AppCompatActivity implements PullRequestView,
        ItemClickListener<PullRequestDataResult> {

    @BindView(R.id.rv_pull_request)
    RecyclerView rvPullRequests;
    
    @BindView(R.id.progress_pull_request)
    ProgressBar pbFetchPullRequest;

    @BindView(R.id.tv_tag_status)
    TextView tvTagTex;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    PullRequestPresenter pullRequestPresenter;

    @Inject
    PullRequestAdapter pullRequestAdapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pull_request_screen);
        this.setupInject();
        this.setupButterKnife();
        this.setupRecyclerView();
        this.pullRequestPresenter.setView(this);
        this.pbFetchPullRequest.setVisibility(View.VISIBLE);
        this.pullRequestPresenter.onViewCreated();
    }

    private void setupInject() {
        DaggerGitRepositoryComponent.builder()
                .appComponent(((MobileApplication) getApplication()).getAppInjection()).build()
                .inject(this);
    }

    private void setupButterKnife() {
        ButterKnife.bind(this);
    }

    @Override
    public void setupToolbar(String title) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                this.getApplicationContext()
        );

        this.rvPullRequests.setAdapter(this.pullRequestAdapter);
        this.rvPullRequests.setLayoutManager(linearLayoutManager);

        this.rvPullRequests.addItemDecoration(
                new SimpleDividerItemDecoration(this.getApplicationContext())
        );

        this.pullRequestAdapter.setItemClickListener(this);
    }

    @Override
    public Bundle getArguments() {
        return getIntent().getExtras();
    }


    @Override
    public void showPullRequestStateTag(int openStateCount, int closeStateCount) {
        String text = String.format(getString(R.string.status_tag), openStateCount, closeStateCount);
        tvTagTex.setText(text);
    }

    @Override
    public void onPullRequestsLoaded(PullRequestDataResult.PullRequestDataList
                                                 pullRequestDataResultList) {
        this.pbFetchPullRequest.setVisibility(View.GONE);
        this.pullRequestAdapter.setPullRequestDataResults(pullRequestDataResultList);
        this.pullRequestAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestError() {
        this.pbFetchPullRequest.setVisibility(View.GONE);
        Toast.makeText(this.getApplicationContext(), getString(R.string.msg_load_pull_request_error),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int resId, PullRequestDataResult item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getHtmlUrl()));
        startActivity(browserIntent);
    }
}
