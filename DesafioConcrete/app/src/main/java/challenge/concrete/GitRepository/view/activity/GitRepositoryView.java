package challenge.concrete.GitRepository.view.activity;

import challenge.concrete.core.view.CoreView;
import challenge.concrete.service.repositories.result.RepositoryDataResult;

public interface GitRepositoryView extends CoreView {
    void onRepositoriesLoaded(RepositoryDataResult repositoryDataResult);

    void onRequestError();

    void onPaginatedDataLoaded(RepositoryDataResult repositoryDataResult);

    void onLoadMoreDataRequestError();

}
