package challenge.concrete.GitRepository.presenter;

import challenge.concrete.GitRepository.view.activity.PullRequestView;
import challenge.concrete.core.presenter.AbstractPresenter;

public interface PullRequestPresenter extends AbstractPresenter<PullRequestView> {
    void onViewCreated();
}
