package challenge.concrete.GitRepository.view;

public interface ItemClickListener<T> {

    void onItemClick(int resId, T item);
}
