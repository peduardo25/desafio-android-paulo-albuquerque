package challenge.concrete.GitRepository.repository.impl;


import javax.inject.Inject;

import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.service.repositories.request.PullRRequest;
import challenge.concrete.service.repositories.request.RepositoryRequest;
import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryDataResult;
import rx.Observable;

public class GitRepositoryRepoImpl implements GitRepositoryRepo {

    private static final String REPOSITORY_LANGUAGE = "language:Java";
    private static final String REPOSITORY_SORT = "stars";

    private RepositoryRequest repositoryRequest;

    private PullRRequest pullRRequest;

    @Inject
    public GitRepositoryRepoImpl(RepositoryRequest repositoryRequest,
                                 PullRRequest pullRRequest) {
        this.repositoryRequest = repositoryRequest;
        this.pullRRequest = pullRRequest;
    }


    @Override
    public Observable<RepositoryDataResult> retrieveJavaRepositories() {
        return this.repositoryRequest.with(REPOSITORY_LANGUAGE, REPOSITORY_SORT, 1).execute();
    }

    @Override
    public Observable<RepositoryDataResult> retrieveJavaRepositoriesFromPage(final int nextPage) {
        return this.repositoryRequest.with(REPOSITORY_LANGUAGE, REPOSITORY_SORT, nextPage).execute();
    }

    @Override
    public Observable<PullRequestDataResult.PullRequestDataList> retrievePullRequests(final String owner,
                                                                  final String repo) {
        return this.pullRRequest.with(owner, repo).execute();

    }
}
