package challenge.concrete.GitRepository.view.activity;

import challenge.concrete.core.view.CoreView;
import challenge.concrete.service.repositories.result.PullRequestDataResult;

public interface PullRequestView extends CoreView {
    void setupToolbar(String title);

    void showPullRequestStateTag(int openStateCount, int closeStateCount);

    void onPullRequestsLoaded(PullRequestDataResult.PullRequestDataList pullRequestDataResultList);

    void onRequestError();
}
