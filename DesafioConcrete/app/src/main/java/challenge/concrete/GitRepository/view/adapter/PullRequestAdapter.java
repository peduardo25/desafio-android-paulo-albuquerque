package challenge.concrete.GitRepository.view.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import challenge.concrete.GitRepository.view.ItemClickListener;
import challenge.concrete.R;
import challenge.concrete.core.util.DateUtil;
import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryItemResult;

public class PullRequestAdapter extends  RecyclerView.Adapter<PullRequestAdapter.PullRequestHolder>{


    private PullRequestDataResult.PullRequestDataList pullRequestDataResults =
            new PullRequestDataResult.PullRequestDataList();

    private ItemClickListener<PullRequestDataResult> itemClickListener;


    @Inject
    DateUtil dateUtil;

    @Inject
    public PullRequestAdapter() {
    }

    @Override
    public PullRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PullRequestAdapter.PullRequestHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.pull_request_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PullRequestHolder holder, int position) {
        PullRequestDataResult itemResult = this.pullRequestDataResults.get(position);
        holder.itemView.setOnClickListener(itemClick(itemResult));
        holder.tvPrName.setText(itemResult.getTitle());
        holder.tvPrDesc.setText(itemResult.getBody());
        holder.tvUsername.setText(itemResult.getUser().getLogin());
        holder.tvPrDate.setText(this.dateUtil.convertDate(itemResult.getCreatedAt()));
        this.setupAvatar(holder, itemResult.getUser().getAvatarUrl());
    }

    private void setupAvatar(final PullRequestHolder holder, final String avatarUrl){
        Glide.with(holder.itemView.getContext())
                .load(avatarUrl)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.ic_profile)
                .into(new BitmapImageViewTarget(holder.ivUserAvatar) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(
                                        holder.itemView.getContext().getResources(), resource
                                );
                        circularBitmapDrawable.setCircular(true);
                        holder.ivUserAvatar.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return this.pullRequestDataResults.size();
    }

    public void setPullRequestDataResults(PullRequestDataResult.PullRequestDataList
                                                  pullRequestDataResults) {
        this.pullRequestDataResults = pullRequestDataResults;
    }

    public void setItemClickListener(ItemClickListener<PullRequestDataResult> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private View.OnClickListener itemClick(PullRequestDataResult itemResult) {
        return view -> {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(view.getId(), itemResult);
            }
        };
    }

    public class PullRequestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_pr_name)
        TextView tvPrName;
        @BindView(R.id.tv_pr_desc)
        TextView tvPrDesc;
        @BindView(R.id.tv_user_login)
        TextView tvUsername;
        @BindView(R.id.tv_pr_date)
        TextView tvPrDate;
        @BindView(R.id.iv_user_avatar)
        ImageView ivUserAvatar;

        public PullRequestHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
