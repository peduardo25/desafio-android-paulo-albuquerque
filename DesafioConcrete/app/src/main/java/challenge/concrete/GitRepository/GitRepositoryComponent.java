package challenge.concrete.GitRepository;

import challenge.concrete.GitRepository.module.GitRepositoryModule;
import challenge.concrete.GitRepository.view.activity.impl.GitRepositoryActivity;
import challenge.concrete.GitRepository.view.activity.impl.PullRequestActivity;
import challenge.concrete.core.injection.AppComponent;
import challenge.concrete.core.scope.ActivityScope;
import dagger.Component;

@Component(modules = {GitRepositoryModule.class }, dependencies = {AppComponent.class})
@ActivityScope
public interface GitRepositoryComponent {

    void inject (GitRepositoryActivity activity);

    void inject (PullRequestActivity activity);
}
