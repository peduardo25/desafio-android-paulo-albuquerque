package challenge.concrete.GitRepository.view.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import challenge.concrete.GitRepository.view.ItemClickListener;
import challenge.concrete.R;
import challenge.concrete.service.repositories.result.RepositoryItemResult;

public class RepositoryAdapter extends  RecyclerView.Adapter<RepositoryAdapter.RepositoryHolder>{

    private List<RepositoryItemResult> repositoryItemResults = new ArrayList<>();

    private ItemClickListener<RepositoryItemResult> itemClickListener;

    @Inject
    public RepositoryAdapter() {
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepositoryHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.repository_screen_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {
        RepositoryItemResult itemResult = this.repositoryItemResults.get(position);
        holder.itemView.setOnClickListener(itemClick(itemResult));
        holder.tvRepositoryName.setText(itemResult.getFullName());
        holder.tvForkCount.setText(String.valueOf(itemResult.getForksCount()));
        holder.tvRepositoryDescription.setText(itemResult.getDescription());
        holder.tvStarCount.setText(String.valueOf(itemResult.getStarsCount()));
        holder.tvUsername.setText(itemResult.getOwnerResult().getLogin());

        this.setupProfileImage(holder, itemResult.getOwnerResult().getAvatarUrl());

    }

    private void setupProfileImage(final RepositoryHolder holder,  final String avatarUrl ) {
        Glide.with(holder.itemView.getContext())
                .load(avatarUrl)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.ic_profile)
                .into(new BitmapImageViewTarget(holder.imgUserAvatar) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(
                                        holder.itemView.getContext().getResources(), resource
                                );
                        circularBitmapDrawable.setCircular(true);
                        holder.imgUserAvatar.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return this.repositoryItemResults.size();
    }

    public void setRepositoryItemResults(final List<RepositoryItemResult> repositoryItemResults) {
        this.repositoryItemResults = repositoryItemResults;
    }

    private View.OnClickListener itemClick(RepositoryItemResult itemResult) {
        return view -> {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(view.getId(), itemResult);
            }
        };
    }

    public void setItemClickListener(ItemClickListener<RepositoryItemResult> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public List<RepositoryItemResult> getRepositoryItemResults() {
        return repositoryItemResults;
    }

    public class RepositoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_rep_name) TextView tvRepositoryName;
        @BindView(R.id.tv_rep_description) TextView tvRepositoryDescription;
        @BindView(R.id.tv_fork_count) TextView tvForkCount;
        @BindView(R.id.tv_star_count) TextView tvStarCount;
        @BindView(R.id.imv_user_avatar) ImageView imgUserAvatar;
        @BindView(R.id.tv_username) TextView tvUsername;

        public RepositoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
