package challenge.concrete.GitRepository.repository;

import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryDataResult;
import rx.Observable;

public interface GitRepositoryRepo {

    Observable<RepositoryDataResult> retrieveJavaRepositories();
    Observable<RepositoryDataResult> retrieveJavaRepositoriesFromPage(int nextPage);

    Observable<PullRequestDataResult.PullRequestDataList> retrievePullRequests(String owner,
                                                           String repo);
}
