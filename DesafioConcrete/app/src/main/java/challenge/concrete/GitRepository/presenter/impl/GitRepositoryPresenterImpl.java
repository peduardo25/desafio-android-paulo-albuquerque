package challenge.concrete.GitRepository.presenter.impl;

import javax.inject.Inject;

import challenge.concrete.GitRepository.presenter.GitRepositoryPresenter;
import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.GitRepository.view.activity.GitRepositoryView;
import challenge.concrete.core.presenter.impl.AbstractPresenterImpl;

public class GitRepositoryPresenterImpl extends AbstractPresenterImpl<GitRepositoryView>
        implements GitRepositoryPresenter {


    private GitRepositoryRepo gitRepositoryRepo;

    @Inject
    public GitRepositoryPresenterImpl(GitRepositoryRepo gitRepositoryRepo) {
        this.gitRepositoryRepo = gitRepositoryRepo;
    }

    @Override
    public void onViewCreated() {

        this.gitRepositoryRepo.retrieveJavaRepositories().subscribe(repositoryDataResult -> {
            mView.onRepositoriesLoaded(repositoryDataResult);
        }, throwable -> {
            mView.onRequestError();
        });
    }

    @Override
    public void onLoadMore(int nextPage) {
        this.gitRepositoryRepo.retrieveJavaRepositoriesFromPage(nextPage).subscribe(
                repositoryDataResult -> {
                    mView.onPaginatedDataLoaded(repositoryDataResult);
                }, throwable -> {
                    mView.onLoadMoreDataRequestError();
                });
    }
}
