package challenge.concrete.GitRepository.module;

import challenge.concrete.GitRepository.presenter.GitRepositoryPresenter;
import challenge.concrete.GitRepository.presenter.PullRequestPresenter;
import challenge.concrete.GitRepository.presenter.impl.GitRepositoryPresenterImpl;
import challenge.concrete.GitRepository.presenter.impl.PullRequestPresenterImpl;
import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.GitRepository.repository.impl.GitRepositoryRepoImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class GitRepositoryModule {

    @Provides
    public GitRepositoryPresenter gitRepositoryPresenterProvider(GitRepositoryPresenterImpl impl){
        return impl;
    }

    @Provides
    public PullRequestPresenter getPullRequestPresenterProvider(PullRequestPresenterImpl impl){
        return impl;
    }

    @Provides
    public GitRepositoryRepo gitRepositoryRepoProvider(GitRepositoryRepoImpl impl){
        return impl;
    }


}
