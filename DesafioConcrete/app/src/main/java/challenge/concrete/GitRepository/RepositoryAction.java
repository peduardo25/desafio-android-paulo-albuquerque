package challenge.concrete.GitRepository;

public interface RepositoryAction {

    String REPOSITORY_KEY = "rep_key";

    interface Activity {
        String PULL_REQUEST_ACTIVITY = "challenge.concrete.GitRepository.view.activity.impl.PullRequestActivity";
    }
}
