package challenge.concrete.GitRepository.presenter;

import challenge.concrete.GitRepository.view.activity.GitRepositoryView;
import challenge.concrete.core.presenter.AbstractPresenter;

public interface GitRepositoryPresenter extends AbstractPresenter<GitRepositoryView> {
    void onViewCreated();

    void onLoadMore(int nextPage);
}
