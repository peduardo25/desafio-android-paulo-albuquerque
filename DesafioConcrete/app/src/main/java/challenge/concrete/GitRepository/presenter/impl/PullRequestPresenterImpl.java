package challenge.concrete.GitRepository.presenter.impl;

import javax.inject.Inject;

import challenge.concrete.GitRepository.RepositoryAction;
import challenge.concrete.GitRepository.presenter.PullRequestPresenter;
import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.GitRepository.view.activity.PullRequestView;
import challenge.concrete.core.presenter.impl.AbstractPresenterImpl;
import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryItemResult;

public class PullRequestPresenterImpl extends AbstractPresenterImpl<PullRequestView>
        implements PullRequestPresenter {


    private static final String OPEN_STATE = "open";
    private GitRepositoryRepo gitRepositoryRepo;

    @Inject
    public PullRequestPresenterImpl(GitRepositoryRepo gitRepositoryRepo) {
        this.gitRepositoryRepo = gitRepositoryRepo;
    }

    @Override
    public void onViewCreated(){
        RepositoryItemResult repData = mView.getArguments().
                getParcelable(RepositoryAction.REPOSITORY_KEY);
        if (repData != null) {
            this.mView.setupToolbar(repData.getFullName());
            this.fetchData(repData);
        }
    }

    private void fetchData(final RepositoryItemResult repData) {
        this.gitRepositoryRepo.retrievePullRequests(
                repData.getOwnerResult().getLogin(), repData.getName()).subscribe(
                pullRequestDataResultList -> {
                    mView.onPullRequestsLoaded(pullRequestDataResultList);
                    calculatePullRequestStatus(pullRequestDataResultList);

                }, throwable -> {
                    mView.onRequestError();
                });
    }

    private void calculatePullRequestStatus(PullRequestDataResult.PullRequestDataList
                                                    pullRequestDataResultList) {

        int openStateCount = 0;
        int closeStateCount = 0;
        for ( PullRequestDataResult data : pullRequestDataResultList) {
            if (data.getState().equals(OPEN_STATE)) {
                openStateCount++;
            } else {
                closeStateCount++;
            }
        }

        this.mView.showPullRequestStateTag(openStateCount, closeStateCount);

    }
}
