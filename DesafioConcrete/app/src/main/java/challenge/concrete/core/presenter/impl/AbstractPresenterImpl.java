package challenge.concrete.core.presenter.impl;

import android.os.Bundle;

import challenge.concrete.core.view.CoreView;

public class AbstractPresenterImpl <T extends CoreView> {

    protected T mView;

    public void setView(T mView) {
        this.mView = mView;
    }

    public void onViewCreated(Bundle savedInstanceState) {
        //Used in child class if needed
    }
}
