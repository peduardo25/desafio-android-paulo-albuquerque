package challenge.concrete.core.injection.module;

import android.content.Context;
import android.content.res.Resources;

import com.octo.android.robospice.SpiceManager;

import javax.inject.Singleton;

import challenge.concrete.core.application.MobileApplication;
import challenge.concrete.service.core.service.ChallengeRetrofitSpiceService;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private MobileApplication mobileApplication;

    public AppModule(final MobileApplication mobileApplication) {
        this.mobileApplication = mobileApplication;
    }

    @Provides
    @Singleton
    MobileApplication mobileApplicationProvider() {
        return this.mobileApplication;
    }

    @Provides
    @Singleton
    Context contextProvider() {
        return this.mobileApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources resourcesProvider() {
        return this.mobileApplication.getResources();
    }

    @Provides
    @Singleton
    SpiceManager spiceManagerProvider() {
        return new SpiceManager(ChallengeRetrofitSpiceService.class) {
            @Override
            public synchronized void shouldStop() {
                if (isStarted() &&
                        getPendingRequestCount() == 0 &&
                        getRequestToLaunchCount() == 0) {
                    super.shouldStop();
                }
            }

            @Override
            public synchronized void start(Context context) {
                if (!isStarted()) {
                    super.start(context);
                }
            }
        };
    }

}
