package challenge.concrete.core.presenter;

import android.os.Bundle;

import challenge.concrete.core.view.CoreView;

public interface AbstractPresenter<T extends CoreView>  {

    void setView(T mView);

    void onViewCreated(Bundle savedInstanceState);

}
