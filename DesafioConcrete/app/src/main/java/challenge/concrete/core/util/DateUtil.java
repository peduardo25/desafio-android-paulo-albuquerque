package challenge.concrete.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

public class DateUtil {

    private static final String APP_FORMAT = "dd/MM/yyyy";
    private static final String API_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @Inject
    public DateUtil() {

    }

    public String convertDate(final String apiDate) {

        SimpleDateFormat sdf = new SimpleDateFormat(API_FORMAT);
        Date date;
        try {
            date = sdf.parse(apiDate);
        } catch (ParseException e) {
            return "";
        }

        sdf.applyPattern(APP_FORMAT);

        return sdf.format(date);

    }

}
