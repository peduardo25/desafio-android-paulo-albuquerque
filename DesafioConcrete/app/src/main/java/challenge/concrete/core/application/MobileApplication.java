package challenge.concrete.core.application;

import android.app.Application;

import challenge.concrete.core.injection.AppComponent;
import challenge.concrete.core.injection.DaggerAppComponent;
import challenge.concrete.core.injection.module.AppModule;

public class MobileApplication extends Application {

    private AppComponent appInjection;

    @Override
    public void onCreate() {
        super.onCreate();
        this.appInjection = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppInjection() {
        return appInjection;
    }
}
