package challenge.concrete.core.injection;


import android.content.Context;
import android.content.res.Resources;

import com.octo.android.robospice.SpiceManager;

import javax.inject.Singleton;

import challenge.concrete.core.application.MobileApplication;
import challenge.concrete.core.injection.module.AppModule;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    MobileApplication getMobileApplication();

    Context getContext();

    SpiceManager getSpiceManager();

    Resources getResources();
}
