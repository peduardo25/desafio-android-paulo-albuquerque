package challenge.concrete.core.view;

import android.os.Bundle;

public interface CoreView {

    Bundle getArguments();
}
