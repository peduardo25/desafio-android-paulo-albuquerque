package challenge.concrete.core.IdlingResource;

import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingResource;

import com.octo.android.robospice.SpiceManager;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

public class SpiceManagerIdleResource implements IdlingResource {

    private SpiceManager spiceManager;

    @Nullable private volatile ResourceCallback mCallback;

    private AtomicBoolean mIsIdleNow = new AtomicBoolean(true);

    @Inject
    public SpiceManagerIdleResource(final SpiceManager spiceManager) {
        this.spiceManager = spiceManager;
    }


    @Override
    public String getName() {
        return SpiceManagerIdleResource.class.getName();
    }

    @Override
    public boolean isIdleNow() {
        final int count = getPendingSpiceManager();
        if (count == 0) {
            setIdleState(true);
            return true;
        }
        setIdleState(false);
        return false;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        mCallback = callback;
    }


    private int getPendingSpiceManager() {
        if (this.spiceManager != null) {
            return this.spiceManager.getPendingRequestCount() + spiceManager.getRequestToLaunchCount()
                    + spiceManager.getPendingRequestCount() + spiceManager.getRequestToLaunchCount();
        }
        return 0;
    }

    public void setIdleState(boolean isIdleNow) {
        mIsIdleNow.set(isIdleNow);
        if (isIdleNow && mCallback != null) {
            mCallback.onTransitionToIdle();
        }
    }
}
