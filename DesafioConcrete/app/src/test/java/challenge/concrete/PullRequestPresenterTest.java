package challenge.concrete;

import android.os.Bundle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import challenge.concrete.GitRepository.presenter.impl.PullRequestPresenterImpl;
import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.GitRepository.view.activity.PullRequestView;
import challenge.concrete.service.repositories.result.OwnerResult;
import challenge.concrete.service.repositories.result.PullRequestDataResult;
import challenge.concrete.service.repositories.result.RepositoryItemResult;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class PullRequestPresenterTest {

    @Mock
    GitRepositoryRepo gitRepositoryRepo;

    @Mock
    PullRequestView pullRequestView;

    @Mock
    Bundle bundle;

    @InjectMocks
    PullRequestPresenterImpl pullRequestPresenter;


    @Test
    public void shouldShowToolbarTitleWhenViewIsCreated(){
        RepositoryItemResult rep = this.createRepMockObject();
        Mockito.when(this.pullRequestView.getArguments()).thenReturn(this.bundle);
        Mockito.when(this.bundle.getParcelable("rep_key")).thenReturn(rep);

        Mockito.when(this.gitRepositoryRepo.retrievePullRequests(rep.getOwnerResult().getLogin(),
                rep.getName())).
                thenReturn(Observable.just(this.createPullRequestListMockObject()));

        this.pullRequestPresenter.setView(this.pullRequestView);
        this.pullRequestPresenter.onViewCreated();

        Mockito.verify(this.pullRequestView).setupToolbar("ConcreteChallengeRep");

    }

    @Test
    public void shouldUpdateViewWhenDataIsLoadedFromServer(){
        RepositoryItemResult rep = this.createRepMockObject();
        PullRequestDataResult.PullRequestDataList list = this.createPullRequestListMockObject();
        Mockito.when(this.pullRequestView.getArguments()).thenReturn(this.bundle);
        Mockito.when(this.bundle.getParcelable("rep_key")).thenReturn(rep);

        Mockito.when(this.gitRepositoryRepo.retrievePullRequests(rep.getOwnerResult().getLogin(),
                rep.getName())).
                thenReturn(Observable.just(list));

        this.pullRequestPresenter.setView(this.pullRequestView);
        this.pullRequestPresenter.onViewCreated();

        Mockito.verify(this.pullRequestView).onPullRequestsLoaded(list);
    }

    @Test
    public void shouldUpdateStatusTagWithProperStatusCount() {
        RepositoryItemResult rep = this.createRepMockObject();
        PullRequestDataResult.PullRequestDataList list = this.createPullRequestListMockObject();
        Mockito.when(this.pullRequestView.getArguments()).thenReturn(this.bundle);
        Mockito.when(this.bundle.getParcelable("rep_key")).thenReturn(rep);

        Mockito.when(this.gitRepositoryRepo.retrievePullRequests(rep.getOwnerResult().getLogin(),
                rep.getName())).
                thenReturn(Observable.just(list));

        this.pullRequestPresenter.setView(this.pullRequestView);
        this.pullRequestPresenter.onViewCreated();

        Mockito.verify(this.pullRequestView).showPullRequestStateTag(2,1);
    }

    private RepositoryItemResult createRepMockObject() {
        final String repName = "ConcreteChallengeRep";
        RepositoryItemResult rep = new RepositoryItemResult();
        OwnerResult ownerResult = new OwnerResult();
        ownerResult.setLogin("mocked login");
        ownerResult.setId(1);
        rep.setFullName(repName);
        rep.setName("repName");
        rep.setOwnerResult(ownerResult);

        return rep;
    }

    private PullRequestDataResult.PullRequestDataList createPullRequestListMockObject() {
        PullRequestDataResult.PullRequestDataList list = new PullRequestDataResult.PullRequestDataList();
        PullRequestDataResult pr1 = new PullRequestDataResult();
        pr1.setBody("BodyTest");
        pr1.setTitle("TitleTest");
        pr1.setState("open");

        PullRequestDataResult pr2 = new PullRequestDataResult();
        pr2.setBody("Body2Test");
        pr2.setTitle("Title2Test");
        pr2.setState("close");

        PullRequestDataResult pr3 = new PullRequestDataResult();
        pr3.setBody("Body3Test");
        pr3.setTitle("Title3Test");
        pr3.setState("open");

        list.add(pr1);
        list.add(pr2);
        list.add(pr3);

        return list;
    }
}
