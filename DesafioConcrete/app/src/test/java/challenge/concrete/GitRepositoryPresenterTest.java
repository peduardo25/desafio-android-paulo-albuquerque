package challenge.concrete;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import challenge.concrete.GitRepository.presenter.impl.GitRepositoryPresenterImpl;
import challenge.concrete.GitRepository.repository.GitRepositoryRepo;
import challenge.concrete.GitRepository.view.activity.GitRepositoryView;
import challenge.concrete.service.repositories.result.RepositoryDataResult;
import challenge.concrete.service.repositories.result.RepositoryItemResult;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class GitRepositoryPresenterTest {

    @Mock
    GitRepositoryRepo gitRepositoryRepo;

    @Mock
    GitRepositoryView gitRepositoryView;

    @InjectMocks
    GitRepositoryPresenterImpl gitRepositoryPresenter;

    @Test
    public void shouldUpdateViewWhenDataIsLoadedFromServer() {

        RepositoryDataResult data = this.createGitMockObject();
        Mockito.when(this.gitRepositoryRepo.retrieveJavaRepositories()).
                thenReturn(Observable.just(data));

        this.gitRepositoryPresenter.setView(this.gitRepositoryView);
        this.gitRepositoryPresenter.onViewCreated();

        Mockito.verify(this.gitRepositoryView).onRepositoriesLoaded(data);
    }

    @Test
    public void shouldUpdateRecyclerListWhenPaginatedRequest() {

        RepositoryDataResult data = this.createGitMockObject();
        Mockito.when(this.gitRepositoryRepo.retrieveJavaRepositoriesFromPage(2)).
                thenReturn(Observable.just(data));

        this.gitRepositoryPresenter.setView(this.gitRepositoryView);
        this.gitRepositoryPresenter.onLoadMore(2);

        Mockito.verify(this.gitRepositoryView).onPaginatedDataLoaded(data);
    }

    private RepositoryDataResult createGitMockObject() {

        RepositoryDataResult data = new RepositoryDataResult();
        List<RepositoryItemResult> list = new ArrayList<>();
        RepositoryItemResult item = new RepositoryItemResult();
        item.setName("test");
        list.add(item);
        data.setItems(list);

        return data;
    }

}
