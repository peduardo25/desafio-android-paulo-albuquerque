package challenge.concrete;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import challenge.concrete.core.util.DateUtil;

@RunWith(MockitoJUnitRunner.class)
public class DateUtilTest {

    @InjectMocks
    DateUtil dateUtil;

    @Test
    public void shouldReturnDateWithAppPattern(){
        String date = this.dateUtil.convertDate("2013-01-08T20:11:48Z");
        Assert.assertEquals("08/01/2013", date);
    }

    @Test
    public void shouldNotReturnEmptyStringWhenThePatternDoesNotMatch (){
        String date = this.dateUtil.convertDate("2013-01-08T20");
        Assert.assertEquals("", date);
    }
}
